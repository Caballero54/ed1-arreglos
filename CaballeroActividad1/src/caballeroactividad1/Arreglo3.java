
package caballeroactividad1;

public class Arreglo3 {

    public static void main(String[] args) {
        String[] Nombres= {"Carlos", "Cinthia","Victor", "Monica", "Yazuri"};
        int[] edad= {21, 20, 22, 24, 23}; 
        
        int suma=0;
        double promedio;
        
        for(int i=0;i<edad.length;i++){
            suma+=edad[i];
        }
        System.out.println("la suma es: "+suma);
        promedio=(double)suma/edad.length;
        System.out.println("El promedio es: "+promedio);
    }
    
}
